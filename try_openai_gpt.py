import openai
#
import os
from dotenv import load_dotenv
#
import urllib.request
#
from PIL import Image, ImageFont, ImageDraw



load_dotenv()

openai.api_key = os.getenv("OPENAI_API_KEY")

def try_hi():
    """
    ref https://platform.openai.com/docs/quickstart/build-your-application
    """
    res = openai.Completion.create(
        model="text-davinci-003",
        prompt='hi',
        temperature=0.6,
    )

    print(res)
    print(res.choices[0].text)


def try_create_image():
    """
    ref https://youtu.be/mcVfw9rB3sQ?t=793
    generated image be stored at /tmp/... <-- view exact location in console output text
    """

    def demo00_gen_img():
        res = openai.Image.create(
            # prompt = 'python+javascript fullstack engineer from asia',
            prompt = '''
            Python. OpenAI GPT. Coding.
            Give me something cool!
            ''',

            # size = '1080x1920',  # instagram reel size  ref https://youtu.be/mcVfw9rB3sQ?t=1097  #NOTE get error  > openai.error.InvalidRequestError: '1080x1920' is not one of ['256x256', '512x512', '1024x1024'] - 'size'
            size = '512x512',

            n = 1,
        )
        img_url = res['data'][0]['url']

        img_ext = img_url.split('?')[0].split('.')[-1]  # eg .png
        img_f   = f'/tmp/try_openai_gpt__create_image.{img_ext}'

        urllib.request.urlretrieve(url=img_url, filename=img_f)

        print(res)
        print(img_url)
        print(img_f)

        return img_f

    def demo01_overlay_text():
        """
        overlay text onto the image
        ref https://youtu.be/mcVfw9rB3sQ?t=947
        ref https://stackoverflow.com/a/16377244/248616
        """
        i      = Image.open(img_f)

        text_str = 'Sample Text'

        color  = (255,255,255)
        # color    = (0,0,0)

        #TODO                       font-file <-- how to get one?
        # font = ImageFont.truetype(font-file,        font-size)
        font = ImageFont.truetype('Pillow/Tests/fonts/FreeMono.ttf', 66)

        i_draw = ImageDraw.Draw(i)
        # text_width,_ = i_draw.textsize(text_str)  #TODO why getting error  > AttributeError: 'ImageDraw' object has no attribute 'textsize'
        # text_pos     = (i.width - text_width - 10, 10)
        text_pos       = (33, 33)  #TODO get textsize() working above to replace this hardcoded

        # draw.text( (x, y), text content,  fill=(r,g,b) text color, font )
        # draw.text( (0, 0), 'Sample Text', (255,255,255), font )
        i_draw.text( text_pos, text_str, color, font)

        textovverlay_f = f'/tmp/textovverlay--{os.path.basename(img_f)}'
        i.save(textovverlay_f)
        print(textovverlay_f)

    img_f = demo00_gen_img()
    # img_f = f'/tmp/try_openai_gpt__create_image.png'  # debug
    # img_f = f'output_d/try_openai_gpt__create_image.png'  # debug

    demo01_overlay_text()


# try_hi()
try_create_image()
